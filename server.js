import express from 'express'

const app = express()

// define a salad and a burger
const salad = { avocado: 1, mango: 1, tomato: 0.2, arugula: true, onion: true }
const burger = { buns: 2, shrimp: 1, egg: 1, lettuce: 2.5, mayo: true }
// define arrays of 100 each
const salads = new Array(100).fill(salad)
const burgers = new Array(100).fill(burger)

// define the routes with the optional count query parameter
app.get('/salads', ({ query: { count } }, res) => res.json(get(salads, count)))
app.get('/burgers', ({ query: { count } }, res) =>
  res.json(get(burgers, count))
)
// helper method to get a slice of the array based on count
const get = (what, count) => what.splice(0, parseInt(count) || 1)
// start the server at localhost:4000
app.listen(4000)
